package controlador;

import java.io.IOException;
import java.util.Locale;

import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class IniciBotonsController extends Application {

	//TEXT
	private ResourceBundle texts;
	
	//ELEMENTS DE LA INTERFICIE
	@FXML private Button btnProveidors;
    @FXML private Button btnClients;
    @FXML private Button btnProductes;
    @FXML private Button btnSortir;
	
	//INICIALITZACIÓ, ES FAN ELS PROCESOS NOMÉS OBRIR LA FINESTRA
	public IniciBotonsController() {
		super();
		//Carregar fitxer de textos multiidioma de la localització actual
//		Locale localitzacioDisplay = Locale.getDefault(Category.DISPLAY);
		Locale localitzacioDisplay =  new Locale("en", "GB");
		texts = ResourceBundle.getBundle("vista.Texts", localitzacioDisplay);
	}

	//INICIALITZACIÓ, ES FAN ELS PROCESOS NOMÉS OBRIR LA FINESTRA
	@Override
	public void start(Stage primaryStage) throws IOException {

		//Carrega el fitxer amb la interficie d'usuari inicial (Scene)
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/IniciBotonsView.fxml"));		
		//assignar el fitxer d'idioma al formulari
		loader.setResources(texts);

		Scene fm_inici = new Scene(loader.load());
		
		//Li assigna la escena a la finestra inicial (primaryStage) i la mostra
		primaryStage.setScene(fm_inici);
		primaryStage.setTitle(texts.getString("txt.agenda"));
		primaryStage.show();

       
	}

	//FUNCIONAMENT DELS BOTONS
	@FXML
	private void onAction(ActionEvent e) throws IOException {

		/**
		 * AL CLICAR A CLIENTS
		 */
		if(e.getSource() == btnClients){			
			
			//Carrega el fitxer amb la interficie d'usuari
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/ClientsView.fxml"));
			//assignar el fitxer d'idioma al formulari
			loader.setResources(texts);
			
			//Crea una nova finestra i l'obre 
			Stage stage = new Stage();
			Scene fmClients = new Scene(loader.load());
			stage.setTitle(texts.getString("txt.clients"));
			stage.setScene(fmClients);
			stage.show();
			
			ClienteController clientsControler = loader.getController();
			clientsControler.setVentana(stage);
			
			//Programem l'event que s'executará quan es tanqui la finestra
			stage.setOnCloseRequest((WindowEvent we) -> {
				clientsControler.sortir();
			});
			
		/**
		 * AL CLICAR A PROVEIDORS
		 */
		}else if(e.getSource() == btnProveidors){			
			
			//Carrega el fitxer amb la interficie d'usuari
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/ProveidorsView.fxml"));
			//assignar el fitxer d'idioma al formulari
			loader.setResources(texts);
			
			//Crea una nova finestra i l'obre 
			Stage stage = new Stage();
			Scene fmProveidors = new Scene(loader.load());
			stage.setTitle(texts.getString("txt.proveidors"));
			stage.setScene(fmProveidors);
			stage.show();
			
			//Crear un objecte de la clase PersonasController ja que necessitarem accedir al mètodes d'aquesta classe
			ProveidorController proveidorsControler = loader.getController();
			proveidorsControler.setVentana(stage);
			
			//Programem l'event que s'executará quan es tanqui la finestra
			stage.setOnCloseRequest((WindowEvent we) -> {
				proveidorsControler.sortir();
			});
			
		/**
		 * AL CLICAR A PRODUCTES
		 */
		}else if(e.getSource() == btnProductes){			
			
			//Carrega el fitxer amb la interficie d'usuari
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/vista/ProductesView.fxml"));
			//assignar el fitxer d'idioma al formulari
			loader.setResources(texts);
			
			//Crea una nova finestra i l'obre 
			Stage stage = new Stage();
			Scene fmProductes = new Scene(loader.load());
			stage.setTitle(texts.getString("txt.productes")); 
			stage.setScene(fmProductes);
			stage.show();
			
			//Crear un objecte de la clase PersonasController ja que necessitarem accedir al mètodes d'aquesta classe
			ProducteController producteControler = loader.getController();
			producteControler.setVentana(stage);
			
			//Programem l'event que s'executará quan es tanqui la finestra
			stage.setOnCloseRequest((WindowEvent we) -> {
				producteControler.sortir();
			});
			
		/**
		 * AL CLICAR A SORTIR
		 */
		}else if(e.getSource() == btnSortir){
			Platform.exit();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
