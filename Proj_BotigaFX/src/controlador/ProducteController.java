package controlador;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TreeSet;

import org.controlsfx.control.CheckComboBox;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Pack;
import model.Producte;
import model.ProducteAbstract;
import model.ProductesDAO;

public class ProducteController{

	//DAO I TEXTS
	private ProductesDAO pd;
	private ResourceBundle texts;

	//ELEMENTS DE LA INTERFICIE
	private Stage ventana;
	@FXML private TextField idTextField;
	@FXML private TextField nomTextField;
	@FXML private TextField preuTextField;
	@FXML private TextField stockTextField;
	@FXML private DatePicker iniciDataPicker;
	@FXML private DatePicker fiDataPicker;
	@FXML private CheckBox packCheckBox;
    @FXML private CheckComboBox<Producte> idesComboBox;
    @FXML private TextField descTextField;
	@FXML private Button btnEliminar;
    @FXML private Button btnGuardar;
    @FXML private Button btnSortir;
    @FXML private AnchorPane root;
    
    //ALTRES
    private ObservableList<Producte> productes;
	private ValidationSupport vs;

	/**
	 * INICIALITZACIÓ, ES FAN ELS PROCESOS NOMÉS OBRIR LA FINESTRA
	 */
	@FXML 
	private void initialize() {
		/**
		 * AFAGA LA LOCALITZACIÓ
		 * CREA UNA VARIABLE PER A LA INTERNACIONALITZACIÓ
		 */
//		Locale localitzacioDisplay = Locale.getDefault(Category.DISPLAY);
		Locale localitzacioDisplay =  new Locale("en", "GB");
		texts = ResourceBundle.getBundle("vista.Texts", localitzacioDisplay);
		
		/**
		 * CREA UN DAO
		 * IMPORTA FITXERS
		 * ELS MOSTRA PER CONSOLA
		 */
		pd = new ProductesDAO();
		pd.obrirFitxer();
		pd.mostrar();
		
		/**
		 * ASSIGNA ELS IDES DELS PRODUCTES AL COMBO-BOX
		 */
		productes = FXCollections.observableArrayList();
		for (ProducteAbstract prod : pd.getMap().values()) {
			if (prod instanceof Producte producte) {
				productes.add(producte);
			}
		}
		idesComboBox.getItems().addAll(productes);
		
		
		/**
		 * VALIDADOR DELS CAMPS (CAMP, OBLIGATORI, MISSATGE) | (CAMP, REGEX, ERROR)
		 */
		vs = new ValidationSupport();
		vs.registerValidator(idTextField, true, Validator.createEmptyValidator(texts.getString("oblig.id")));
		vs.registerValidator(nomTextField, true, Validator.createEmptyValidator(texts.getString("oblig.nom")));
//    	vs.registerValidator(iniciDataPicker, true, Validator.createEmptyValidator("Inici de Catalogament obligatori"));
//    	vs.registerValidator(fiDataPicker, true, Validator.createEmptyValidator("Descatalogament obligatori"));

		vs.registerValidator(idTextField, Validator.createRegexValidator(texts.getString("oblig.format"), "\\d", Severity.ERROR));
	}

	public Stage getVentana() {
		return ventana;
	}

	public void setVentana(Stage ventana) {
		this.ventana = ventana;
	}
	
	//SI COINCIDEIX L'ID ES COL·LOCARÀ AUTOMÀTICAMENT EL PRODUCTE/PACK
    @FXML
    void onKeyReleased(KeyEvent event) {
    	
    	ProducteAbstract producte = pd.buscar(-1);			
    	/**
    	 * ASSIGNA EL RPODUCTE A UNA VARIABLE
    	 */
    	if (idTextField.getText().matches("\\d")) {
    		producte = pd.buscar(Integer.parseInt(idTextField.getText()));			
		} 
		
    	/**
		 * TOT EL PROCÉS DE SELECCIÓ I DESSELECCIÓ JUNT AMB OMPLIR ELS CAMPS NECESSARIS
		 */
		//Si troba producte
		if (producte != null) {
			//Assigna nom
			nomTextField.setText(producte.getNom());
			
			//Si es un producte
			if (producte instanceof Producte p) {
				//Omple els camps corresponents al producte
				preuTextField.setText(String.valueOf(p.getPreuVenda()));
				stockTextField.setText(String.valueOf(p.getStock()));
				iniciDataPicker.setValue(p.getiniciC());
				fiDataPicker.setValue(p.getFinalC());
				
				//Dasabilita els camps corresponents al pack
				packCheckBox.setSelected(false);
				onActionCheckBox(null);
				idesComboBox.getCheckModel().clearChecks();
				descTextField.setText("");
			
			//Si es un pack
			}else if (producte instanceof Pack pk) {
				//Dasabilita els camps corresponents al pack
				preuTextField.setText("");
				stockTextField.setText("");
				iniciDataPicker.setValue(null);
				fiDataPicker.setValue(null);
				
				//Omple els camps corresponents al pack
				packCheckBox.setSelected(true);
				onActionCheckBox(null);
				for (Producte p : pk.getProductes()) {
					idesComboBox.getCheckModel().check(p);
				}
				descTextField.setText(String.valueOf(pk.getPercentatgeDescompte()));
			}
		//Si no troba producte
		}else {
			//Buida tots els camps menys l'ID
			nomTextField.setText("");
			stockTextField.setText("");
			preuTextField.setText("");
			iniciDataPicker.setValue(null);
			fiDataPicker.setValue(null);
			packCheckBox.setSelected(false);
			onActionCheckBox(null);
			idesComboBox.getCheckModel().clearChecks();
			descTextField.setText("");
		}
		
    }	
	
	//CADA VEGADA QUE ES PRESIONI EL CHECKBOX DE PACK
    @FXML
    void onActionCheckBox(ActionEvent event) {
    	/**
    	 * VARIABLE QUE ACTIVARÀ O DESACTIVARÀ ELS CAMPS CORRESPONENTS
    	 */
    	boolean disabled = false;
    	
    	/**
    	 * ASSIGNACIÓ DE LA VARIABLE EN CAS DE PACK
    	 */
    	if (packCheckBox.isSelected()) {
			disabled = true;
			vs.registerValidator(descTextField, Validator.createRegexValidator(texts.getString("oblig.format"), "^\\d*\\.\\d+|\\d+\\.\\d*$", Severity.ERROR));

		}
    	
    	/**
    	 * ACTIVACIÓ/DESACTIVACIÓ DE CAMPS
    	 */
    	preuTextField.setDisable(disabled);
    	stockTextField.setDisable(disabled);
    	iniciDataPicker.setDisable(disabled);
    	fiDataPicker.setDisable(disabled);
    	
    	idesComboBox.setDisable(!disabled);
    	descTextField.setDisable(!disabled);
    	
    }
	
    //GUARDA EL PACK O PRODUCTE
    @FXML
    void onActionGuardar(ActionEvent event) {

    	/**
    	 * FUNCIONAMENT DE GUARDAT
    	 */
    	//Si es un producte
    	if (!packCheckBox.isSelected() && isDatosValidos()) {
    		//Crea l'objecte
    		Producte p = new Producte();
    		//Assigna tots els valors
			p.setIdproducte(Integer.parseInt(idTextField.getText()));
			p.setNom(nomTextField.getText());
			p.setStock(Integer.parseInt(stockTextField.getText()));
			p.setpreuVenda(Double.parseDouble(preuTextField.getText()));
			p.setiniciC(iniciDataPicker.getValue());
			p.setfinalC(fiDataPicker.getValue());
		
			//Si no existeix i els camps són correctes
			if (pd.buscar(p.getIdproducte()) == null && isDatosValidos()) {
				
				/**
				 * GUARDA EL PRODUCTE, L'AFEGEIX A LA LLISTA DE PRODUCTES I RENOVA EL CHECKCOMBOBOX
				 */
				pd.guardar(p);
				productes.add(p);
				idesComboBox.getItems().clear();
				idesComboBox.getItems().addAll(productes);
				
				//Alerta de producte afegit
				alert(3);
				
			}else {
				//Alerta de producte ja afegit
				alert(1);
			}
			
		//Si es un pack
    	} else {
    		//Crea l'objecte
    		Pack pk = new Pack();    
    		//Assigna tots els valors
    		pk.setIdproducte(Integer.parseInt(idTextField.getText()));
    		pk.setNom(nomTextField.getText());
    		pk.setPercentatgeDescompte(Double.parseDouble(descTextField.getText()));
    		
    		TreeSet<Producte> prods = new TreeSet<>();
    		for (Producte producte : idesComboBox.getCheckModel().getCheckedItems()) {
		        prods.add(producte);
    		}
    		pk.setProductes(prods);

    		//Si no existeix i els camps són correctes
    		if (pd.buscar(pk.getIdproducte()) == null && isDatosValidos()) {
    			
    			/**
    			 * GUARDA EL PACK
    			 */
    			pd.guardar(pk);
    			
    			//Alerta de producte afegit
    			alert(3);
    			
    		}else {
    			//Alerta de producte ja afegit
    			alert(1);
    		}
		}
    	
    	/**
    	 * NETEJA DE FORMULARI
    	 */
    	limpiarFormulario();
    }

    @FXML
    void onActionEliminar(ActionEvent event) {
    	/**
    	 * ASSIGNA EL PRODUCTE A UNA VARIABLE
    	 */
    	ProducteAbstract p = pd.buscar(Integer.parseInt(idTextField.getText()));
    	
    	/**
    	 * FUNCIONAMENT D'ELIMINACIÓ
    	 */
    	//Si el producte no existeix
    	if (p == null) {
    		//Alerta de producte no existent
    		alert(2);
		}else {
			
			/**
			 * ELIMINA EL PRODUCTE CORRESPONENT, L'ELIMINA DE LA LLISTA I RENOVA EL CHECKCOMBOBOX
			 */
			pd.eliminar(p.getIdproducte());
			productes.remove(p);
			idesComboBox.getItems().clear();
			idesComboBox.getItems().addAll(productes);

			//Alerta de producte eliminat
			alert(4);
			
			/**
			 * NETEJA EL FORMULARI
			 */
			limpiarFormulario();
		}
    }
    
    //SURT DEL PROGRAMA, GUARDA EL FITXER I TANCA LA FINESTRA
	@FXML 
	private void onActionSortir(ActionEvent e) throws IOException {

		/**
		 * SURT DEL PROGRAMA I GUARDA EL FITXER
		 */
		sortir();
		pd.guardarFitxer();

		/**
		 * TANCA LA PESTANYA
		 */
		ventana.close();
	}

	public void sortir(){
		pd.mostrar();
	}
	
    //VALIDA QUE LES DADES QUE S'HAN INTRODUIT SIGUIN VÀLIDES
	private boolean isDatosValidos() {

		/**
		 * COMPROVACIÓ DE DADES VÀLIDES
		 */
		if (vs.isInvalid()) {
			String errors = vs.getValidationResult().getMessages().toString();
			// Mostrar finestra amb els errors
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.initOwner(ventana);
			alert.setTitle(texts.getString("title.error"));
			alert.setHeaderText(texts.getString("txt.dades"));
			alert.setContentText(errors);
			alert.showAndWait();
		
			return false;
		}
		return true;
	}

	//NETEJA EL FORMULARI
	private void limpiarFormulario(){
		idTextField.setText(null);
		nomTextField.setText("");
		stockTextField.setText("");
		preuTextField.setText("");
		iniciDataPicker.setValue(null);
		fiDataPicker.setValue(null);
		idesComboBox.getCheckModel().clearChecks();
		descTextField.setText("");
		
	}
	
	//ALERTES DEL PROGRAMA
    public void alert(int op) {
    	
    	/**
    	 * TIPUS D'ALERTES
    	 */
    	Alert err = new Alert(Alert.AlertType.ERROR);
    	Alert ok = new Alert(Alert.AlertType.INFORMATION);
    	
    	/**
    	 * ALERTA SEGONS LA NECESSITAT
    	 */
    	switch (op) {
		case 1: //Producte Ja Existent
			
			err.setTitle(null);
			err.setHeaderText(texts.getString("title.error"));
			err.setContentText(texts.getString("txt.prodexistent"));
			
			err.showAndWait();
			break;

		case 2: //Afegit Correctament
			
			err.setTitle(null);
			err.setHeaderText(texts.getString("title.error"));
    		err.setContentText(texts.getString("txt.prodtrobat"));
    		
    		err.showAndWait();
			break;
			
		case 3:
			
    		ok.setTitle(null);
			ok.setHeaderText(texts.getString("title.afegit"));
			ok.setContentText(texts.getString("txt.prodafegit"));
			
			ok.showAndWait();
			break;
			
		case 4:
			
    		ok.setTitle(null);
			ok.setHeaderText(texts.getString("title.eliminat"));
			ok.setContentText(texts.getString("txt.prodeliminat"));
			
			ok.showAndWait();
			break;

		default:
			break;
		}
	}
}
