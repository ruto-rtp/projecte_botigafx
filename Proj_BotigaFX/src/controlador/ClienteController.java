package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.ResourceBundle;

import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.ClientsDAO;
import model.Client;

public class ClienteController{

	//DAO I TEXT
	private ClientsDAO cd;
	private ResourceBundle texts;

	//ELEMENTS DE LA INTERFICIE
	private Stage ventana;
	@FXML private TextField idTextField;
	@FXML private TextField dniTextField;
	@FXML private TextField nomTextField;
	@FXML private TextField cognomsTextField;
	@FXML private TextField telefonTextField;
	@FXML private TextField correuTextField;
	@FXML private CheckBox publicitatCheckBox;
	@FXML private Button btnGuardar;
	@FXML private Button btnEliminar;
	
	@FXML private TableView<Client> tblClients;
	@FXML private TableColumn<Client, Integer> colId;
	@FXML private TableColumn<Client, String> colNom;
    @FXML private TableColumn<Client, String> colCognom;
    @FXML private TableColumn<Client, String> colDni;
    @FXML private TableColumn<Client, String> colTelf;
    @FXML private TableColumn<Client, String> colCorreu;
    @FXML private TableColumn<Client, Boolean> colPub;
    @FXML private AnchorPane root;
    
    //ALTRES
    private ObservableList<Client> clients;
	private ValidationSupport vs;
	
	//INICIALITZACIÓ, ES FAN ELS PROCESOS NOMÉS OBRIR LA FINESTRA
	@FXML 
	private void initialize() {
		/**
		 * AFAGA LA LOCALITZACIÓ
		 * CREA UNA VARIABLE PER A LA INTERNACIONALITZACIÓ
		 */
//		Locale localitzacioDisplay = Locale.getDefault(Category.DISPLAY);
		Locale localitzacioDisplay =  new Locale("en", "GB");
		texts = ResourceBundle.getBundle("vista.Texts", localitzacioDisplay);
		
		/**
		 * CREA UN DAO
		 * IMPORTA FITXERS
		 * ELS MOSTRA PER CONSOLA
		 */
		cd = new ClientsDAO();
		cd.obrirFitxer();
		cd.mostrar();
		
		/**
		 * ASSIGNA ELS ELEMENTS A LA TAULA
		 */
		ArrayList<Client> llistaClients = new ArrayList<>(cd.getMap().values());
		clients = FXCollections.observableArrayList(llistaClients);
		colId.setCellValueFactory(new PropertyValueFactory<>("idpersona"));
		colDni.setCellValueFactory(new PropertyValueFactory<>("dni"));
		colNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
		colCognom.setCellValueFactory(new PropertyValueFactory<>("cognom"));
		colCorreu.setCellValueFactory(new PropertyValueFactory<>("email"));	

		colTelf.setCellValueFactory(new PropertyValueFactory<>("telefon"));
		colPub.setCellValueFactory(new PropertyValueFactory<>("enviarPublicitat"));
		tblClients.setItems(clients);

    	/**
    	 * ASSIGNA EL PRODUCTE A UNA VARIABLE
    	 */
		vs = new ValidationSupport();
		vs.registerValidator(idTextField, true, Validator.createEmptyValidator(texts.getString("oblig.id")));
		vs.registerValidator(nomTextField, true, Validator.createEmptyValidator(texts.getString("oblig.nom")));
		vs.registerValidator(cognomsTextField, true, Validator.createEmptyValidator(texts.getString("oblig.cognom")));
		vs.registerValidator(dniTextField, true, Validator.createEmptyValidator(texts.getString("oblig.dni")));

		vs.registerValidator(correuTextField, Validator.createRegexValidator(texts.getString("oblig.format"), "([a-z A-Z]*)(@)([a-z A-Z]*)(.)([a-z A-Z]*)", Severity.ERROR));
		vs.registerValidator(dniTextField, Validator.createRegexValidator(texts.getString("oblig.format"), "[\\d]{8}([a-z A-Z]{1})", Severity.ERROR));
        vs.registerValidator(telefonTextField, Validator.createRegexValidator(texts.getString("oblig.format"), "([+][0-9]{2})?(\\s)?([0-9]{9})?", Severity.ERROR));
        
	}

	public Stage getVentana() {
		return ventana;
	}

	
	public void setVentana(Stage ventana) {
		this.ventana = ventana;
	}
	
    //AL CLICAR D'AMUNT D'UN CLIENT DE LA TAULA
   
    //AL CLICAR D'AMUNT D'UN CLIENT DE LA TAULA
	@FXML
    void onMouseClickedTblClients(MouseEvent event) {

    	/**
    	 * ASSIGNA EL CLIENT A UNA VARIABLE
    	 */
    	Client c = tblClients.getSelectionModel().getSelectedItem();
    	
    	//Si el client existeix
    	if (c != null) {
    		//Assigna els valors als camps corresponents
    		idTextField.setText(String.valueOf(c.getIdpersona()));
    		nomTextField.setText(c.getNom());
    		cognomsTextField.setText(c.getCognom());
    		dniTextField.setText(c.getDni());
    		telefonTextField.setText(c.getTelefon());
    		correuTextField.setText(c.getEmail());
			publicitatCheckBox.setSelected(c.getEnviarPublicitat());
		}
    }
    
    //GUARDA EL CLIENT
    
    //GUARDA EL CLIENTS
	@FXML
    void onActionGuardar(ActionEvent event) {

    	/**
    	 * ASSIGNA EL CLIENT EN UNA VARIABLE
    	 */
    	//Crea l'objecte
    	Client c = new Client();
    	//Assigna tots els valors
    	c.setIdpersona(Integer.parseInt(idTextField.getText()));
    	c.setDni(dniTextField.getText());
    	c.setNom(nomTextField.getText());
    	c.setCognom(cognomsTextField.getText());
    	c.setTelefon(new LinkedHashSet<>(Arrays.asList(telefonTextField.getText())));
    	c.setDataNeix(null);
    	c.setDireccion(null);
    	c.setEmail(correuTextField.getText());
    	c.setEnviarPublicitat(publicitatCheckBox.isSelected());
    	
    	/**
    	 * FUNCIONAMENT DE GUARDAT
    	 */
    	//Si la taula no conté el client, les dades són valides o el client no existeix
    	if (!clients.contains(c) && isDatosValidos() && cd.buscar(c.getIdpersona()) == null) {
    		
			/**
			 * GUARDA EL PRODUCTE, L'AFEGEIX A LA LLISTA DE PRODUCTES I RENOVA LA TAULA
			 */
			cd.guardar(c);
			clients.add(c);
			tblClients.setItems(clients);
			
			//Alerta de persona afegida
			alert(3);
			
    	}else {
    		//Alerta de persona ja afegida
    		alert(1);
    	}
			
    	/**
    	 * NETEJA EL FORMULARI
    	 */
    	limpiarFormulario();
    }

    //ELIMINA EL CLIENT
    
    //ELIMINA EL CLIENT
	@FXML
    void onActionEliminar(ActionEvent event) {
    	/**
    	 * ASSIGNA EL CLIENT A UNA VARIABLE
    	 */
    	Client c = tblClients.getSelectionModel().getSelectedItem();
    	
    	/**
    	 * FUNCIONAMENT D'ELIMINACIÓ
    	 */
    	//Si el client no existeix
    	if (c == null) {
    		//Alerta de client no existent
    		alert(2);
		}else {
			
			/**
			 * ELIMINA EL CLIENT CORRESPONENT, L'ELIMINA DE LA LLISTA I RENOVA LA TAULA
			 */
			cd.eliminar(c.getIdpersona());
			clients.remove(c);
			tblClients.refresh();
			
			//Alerta de producte eliminat
			alert(4);
			
			/**
			 * NETEJA EL FORMULARI
			 */
			limpiarFormulario();
		}
    }

    //SURT DEL PROGRAMA, GUARDA EL FITXER I TANCA LA FINESTRA
	
    //SURT DEL PROGRAMA, GUARDA EL FITXER I TANCA LA FINESTRA
	@FXML 
	private void onActionSortir(ActionEvent e) throws IOException {

		/**
		 * SURT DEL PROGRAMA I GUARDA EL FITXER
		 */
		sortir();
		cd.guardarFitxer();

		/**
		 * TANCA LA PESTANYA
		 */
		ventana.close();
	}

	
	public void sortir(){
		cd.mostrar();
	}
    
    
	//VALIDA QUE LES DADES QUE S'HAN INTRODUIT SIGUIN VÀLIDES
	private boolean isDatosValidos() {

		/**
		 * COMPROVACIÓ DE DADES VÀLIDES
		 */
		if (vs.isInvalid()) {
			String errors = vs.getValidationResult().getMessages().toString();
			// Mostrar finestra amb els errors
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(ventana);
			alert.setTitle(texts.getString("title.error"));
			alert.setHeaderText(texts.getString("txt.dades"));
			alert.setContentText(errors);
			alert.showAndWait();
		
			return false;
		}
		return true;
	} 

	//NETEJA EL FORMULARI
	//NETEJA EL FORMULARI
	private void limpiarFormulario(){
		idTextField.setText("");
		dniTextField.setText("");
		nomTextField.setText("");
		cognomsTextField.setText("");
		correuTextField.setText("");
		telefonTextField.setText("");
	}
	
	//ALERTES DEL PROGRAMA
    
	//ALERTES DEL PROGRAMA
	public void alert(int op) {
    	
    	/**
    	 * TIPUS D'ALERTES
    	 */
    	Alert err = new Alert(Alert.AlertType.ERROR);
    	Alert ok = new Alert(Alert.AlertType.INFORMATION);
    	
    	/**
    	 * ALERTA SEGONS LA NECESSITAT
    	 */
    	switch (op) {
		case 1: //Producte Ja Existent
			
			err.setTitle(null);
			err.setHeaderText(texts.getString("title.error"));
			err.setContentText(texts.getString("txt.perexistent"));
			
			err.showAndWait();
			break;

		case 2: //Afegit Correctament
			
			err.setTitle(null);
    		err.setHeaderText(texts.getString("title.error"));
    		err.setContentText(texts.getString("txt.pertrobat"));
    		
    		err.showAndWait();
			break;
			
		case 3:
			
    		ok.setTitle(null);
			ok.setHeaderText(texts.getString("title.afegit"));
			ok.setContentText(texts.getString("txt.perafegit"));
			
			ok.showAndWait();
			break;
			
		case 4:
			
    		ok.setTitle(null);
			ok.setHeaderText(texts.getString("title.eliminat"));
			ok.setContentText(texts.getString("txt.pereliminat"));
			
			ok.showAndWait();
			break;

		default:
			break;
		}
	}
}
