package model;

import java.util.Objects;
import java.util.TreeSet;

public final class Pack extends ProducteAbstract{
	
	//ID
	private static final long serialVersionUID = 1L;
	
	
	//VARIABLES
	private TreeSet<Producte> productes = new TreeSet<>(new Comparador());	
	private double percentatgeDescompte;
	
	
	//CONSTRUCTORS
	public Pack() {
		super(-1, "");	
	}
	
	public Pack(int id, String nom, double percentatgeDescompte, TreeSet<Producte> productes) {
		super(id, nom);

		this.percentatgeDescompte = percentatgeDescompte;
		this.productes = productes;
	}
	
	
	//GETTERS I SETTERS
	public double getPercentatgeDescompte() {
		return percentatgeDescompte;
	}

	public void setPercentatgeDescompte(double percentatgeDescompte) {
		this.percentatgeDescompte = percentatgeDescompte;
	}
	
	public TreeSet<Producte> getProductes() {
		return productes;
	}
	
	public void setProductes(TreeSet<Producte> productes) {
		this.productes = productes;
	}
	
	//MÈTODES
	public void afegirProducte(Producte producte) {
		productes.add(producte);
	}
	
	public void esborrarProducte(int idproducte) {
		productes.remove(new Producte(idproducte, "", 0, 0.0, null, null));
	}
	
	
	//MÈTODES
	@Override
	public int hashCode() {
		return Objects.hash(productes);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			
			return true;
		
		if (!super.equals(obj))
			
			return false;
		
		if (getClass() != obj.getClass())
		
			return false;
		
		Pack other = (Pack) obj;
		return Objects.equals(productes, other.productes);
	}

	//PRINT
	@Override
	public String toString() {
		
		String f = "ID pack: " + getIdproducte() + "\nNom pack: " + getNom();
		
		String products = "";
		
		for (Producte producte: productes) {
			products += "\n·ID prod: " + producte.getIdproducte() 
						+ "\n·Nom prod: " + producte.getNom();
		}
		
		return f
				+ products
				+ "\n";
	}
}
