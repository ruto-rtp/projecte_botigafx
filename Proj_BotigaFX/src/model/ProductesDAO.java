package model;

import java.io.*;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

public class ProductesDAO implements Persistable<ProducteAbstract>, Serializable{

	//ID Serialització i HashMap
	private static final long serialVersionUID = 1L;
	private static HashMap<Integer, ProducteAbstract> productes = new HashMap<>();

	//Guardar producte
	@Override
	public ProducteAbstract guardar(ProducteAbstract obj) {
		
		//Guardar a HashMap
		productes.put(obj.getIdproducte(), obj);
		return null;
	}
	
	//Eliminar producte
	@Override
	public ProducteAbstract eliminar(Integer id) {
		
		//Comprovació d'ID
		if (productes.containsKey(id)) {
			//Asigna el producte a una variable
			ProducteAbstract pa = productes.get(id);
			//Elimina el producte
			productes.remove(id);
			
			//Retorna el producte
			return pa;
		}
		return null;
	}
	
	//Buscar producte
	@Override
	public ProducteAbstract buscar(Integer id) {

		//Comprovació d'ID
		if (productes.containsKey(id)) {
			//Retorna el producte
			return productes.get(id);
		}
		return null;
	}
	
	//Agafa el HashMap
	@Override
	public HashMap<Integer, ProducteAbstract> getMap() {
		
		//Retorna el HashMap
		return productes;
	}
	
	public void mostrar(){

		System.out.println("-------------------");
		System.out.println("Productes");
		System.out.println("-------------------");
		
		for (ProducteAbstract p : productes.values()) {
		    System.out.println(p);
		}
	}
	

	//Guardar en un arxiu
	public void guardarFitxer() {
				
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("res/Productes.dat"))){
			
			//Escriu tot el HashMap a l'arxiu
			oos.writeObject(productes);
			
		} catch (Exception e) { //Altres errors
			
			System.out.println("\nUPS!! No shan pogut guardar els productes");
		}
	}
	
	//Carregar de l'arxiu
	public void obrirFitxer() {
		
		//Arxiu
		File f = new File("res/Productes.dat");
		if (f.exists()) { //Només si existeix
		
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){
								
				//Es carreguen els productes al HashMap
				productes = (HashMap<Integer, ProducteAbstract>)ois.readObject();			

			} catch (FileNotFoundException e) { //Arxiu no trobat
	
				//e.printStackTrace();
				System.out.println("\nUPS!! No sha trobat l'arxiu");
			
			}catch (Exception e) { //Altres errors
				
				//e.printStackTrace();
				System.out.println("\nUPS!! Alguna cosa ha fallat (Productes)");
				
			}
		}
	}
	
	//Imprimeix productes descatalogats
	public void imprimitDescatalogats(LocalDate data, NumberFormat nF) {
				
		for (ProducteAbstract prods : productes.values()) {
			if (prods instanceof Producte p && (p.getFinalC().compareTo(data) < 0)) {
				
				int dif = (int)Math.abs(ChronoUnit.DAYS.between(p.getFinalC(), data));
				int dia = (int)Math.abs(ChronoUnit.DAYS.between(p.getFinalC(), data));
				int mes = 0;
				int any = 0;
				
				if (dif >= 30) {
					dia = dif%30;
					mes = dif/30;
				} 
				if (mes >=12) {
					any = mes/12;
					mes = mes%12;
				}
				
				//Es mostra només en dies
				System.out.println(p.getNom() + " des de fa " + nF.format(Math.abs(ChronoUnit.DAYS.between(p.getFinalC(), data))) + " dia/es");
				//Es mostra en dies, mesos i anys
				System.out.println(p.getNom() + " des de fa " + dia + " dia/es " + mes + " mes/os i " + any + " any/s");
			}
		}
	}
}
