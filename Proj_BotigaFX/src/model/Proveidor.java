package model;

import java.time.LocalDate;
import java.util.LinkedHashSet;

public class Proveidor extends Persona{

	//ID
	private static final long serialVersionUID = 1L;
	
	
	//VARIABLES
	private double dtoProntoPago;
	
	
	//CONSTRUCTORS
	public Proveidor() {
		super();
		
		this.dtoProntoPago = 0.00;
	}

	public Proveidor(int id, String dni, String nom, String cognom, LocalDate dataNeix, LinkedHashSet<String> telf, String mail, Adreca direccio, double dtoProntoPago){
		super(id, dni, nom, cognom, dataNeix, telf, mail, direccio);

		this.dtoProntoPago = dtoProntoPago;
	
	}
	
	
	//GETERS I SETTERS
	public double getDtoProntoPago() {
		return dtoProntoPago;
	}

	public void setDtoProntoPago(double dtoProntoPago) {
		this.dtoProntoPago = dtoProntoPago;
	}
	
	
	//PRINT
	@Override
	public String toString() {
		return "\nID: " + getIdpersona()
				+ "\nDNI: " + getDni()
				+ "\nNom: " + getNom() + " " + getCognom()
				+ "\nEdat: " + getEdat()
				+ "\nTelefon: " + getTelefon()
				+ "\nDescompte: " + getDtoProntoPago();
	}
}