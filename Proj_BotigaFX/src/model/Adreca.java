package model;

import java.io.Serializable;

public class Adreca implements Serializable{

	//ID
	private static final long serialVersionUID = 1L;
	
	
	//VARIABLES
	private String poblacio;
	private String provincia;
	private String cp;
	private String domicili;
	
	
	//CONSTRUCTORS
	public Adreca() {
		
	}
	
	public Adreca(String POBLACIO, String PROVINCIA, String CP, String DOMICILI) {
		poblacio = POBLACIO;
		provincia = PROVINCIA;
		cp = CP;
		domicili = DOMICILI;
	}
	
	
	//GETERS I SETTERS
	public void setPoblacio(String poblacio) {
		this.poblacio = poblacio;
	}
	
	public String getPoblacio() {
		return poblacio;
	}
	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getProvincia() {
		return provincia;
	}
	
	public void setCp(String cp) {
		this.cp = cp;
	}
	
	public String getCp() {
		return cp;
	}
	
	public void setDomicili(String domicili) {
		this.domicili = domicili;
	}
	
	public String getDomicili() {
		return domicili;
	}

	
	//PRINT
	public String toString() {
		return "\n -Poblacio: " + poblacio 
				+ "\n -Provincia: " + provincia 
				+ "\n -Codi Postal: " + cp 
				+ "\n -Domicili: " + domicili;
	}	
}