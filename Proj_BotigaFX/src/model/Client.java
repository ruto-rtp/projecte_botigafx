package model;

import java.time.LocalDate;
import java.util.LinkedHashSet;

public class Client extends Persona{
	
	//ID
	private static final long serialVersionUID = 1L;
	
	
	//VARIABLES
	private boolean enviarPublicitat;
	
	
	//CONSTRUCTORS
	public Client() {
		super();
		
		enviarPublicitat = false;
	}

	public Client(int id, String dni, String nom, String cognom, LocalDate dataNeix, LinkedHashSet<String> telef, String mail, Adreca direccio, boolean enviarPublicitat) {
		super(id, dni, nom, cognom, dataNeix, telef, mail, direccio);

		this.enviarPublicitat = enviarPublicitat;
	}
	
	
	//GETERS I SETTERS
	public boolean getEnviarPublicitat() {
		return enviarPublicitat;
	}

	public void setEnviarPublicitat(boolean enviarPublicitat) {
		this.enviarPublicitat = enviarPublicitat;
	}
	
	
	//PRINT
	@Override
	public String toString() {
		return "\nID: " + getIdpersona()
				+ "\nDNI: " + getDni()
				+ "\nNom: " + getNom() + " " + getCognom()
				+ "\nEdat: " + getEdat()
				+ "\nTelefon: " + getTelefon()
				+ "\nRep publicitat: " + getEnviarPublicitat()
				+ "n";
	}

}
