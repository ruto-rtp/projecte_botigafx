package model;

import java.util.Comparator;

public class Comparador implements Comparator<Producte>{

	@Override
	public int compare(Producte p1, Producte p2) {
		return Integer.compare(p1.getIdproducte(), p2.getIdproducte());
	}
	
}
