package model;

import java.util.Comparator;

public class ComparadorPreu implements Comparator<Producte>{

	@Override
	public int compare(Producte p1, Producte p2) {
		return Double.compare(p1.getPreuVenda(), p2.getPreuVenda());
	}
	
}
