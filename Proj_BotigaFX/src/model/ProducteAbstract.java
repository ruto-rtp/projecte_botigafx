package model;

import java.io.Serializable;

public abstract class ProducteAbstract implements Serializable{
	
	//ID
	private static final long serialVersionUID = 1L;
	
	
	//VARIABLES
	private int id;
	private String nom;
	
	
	//CONSTRUCTORS
	public ProducteAbstract() {
	}
	
	public ProducteAbstract(int idproducte, String nom) {
		
		this.id = idproducte;
		this.nom = nom;
	}
	
	
	//GETERS I SETTERS
	public void setIdproducte(int idproducte) {
		this.id = idproducte;
	}
	
	public int getIdproducte() {
		return id;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}
	
	
	//MÈTODES
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			
			return true;
		
		if (obj instanceof ProducteAbstract) {
		
			ProducteAbstract other = (ProducteAbstract) obj;
			return this.nom.equals(other.getNom());
		}
		
		return false;
	}
	
	
	//PRINT
	public String toString() {
		return "\nID:" + getIdproducte()
				+ "\nNom: " + getNom();
	}
}