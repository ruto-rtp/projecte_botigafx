package model;

import java.io.*;
import java.util.HashMap;

public class ClientsDAO implements Persistable<Client>{

	
	private static HashMap<Integer, Client> clients = new HashMap<>();

	//Guardar client
	@Override
	public Client guardar(Client obj) {
		
		//Guardar a HashMap
		clients.put(obj.getIdpersona(), obj);
		return null;
	}
	
	//Eliminar client
	@Override
	public Client eliminar(Integer id) {
		
		//Comprovació d'ID
		if (clients.containsKey(id)) {
			//Asigna el client a una variable
			Client c = (Client)clients.get(id);
			
			//Elimina el producte
			clients.remove(id);
			
			//Retorna el client
			return c;
		}
		return null;
		
	}
	
	//Buscar client
	@Override
	public Client buscar(Integer id) {

		//Comprovació d'ID
		if (clients.containsKey(id)) {
			//Retorna el client
			return clients.get(id);
		}
		return null;
		
	}
	
	//Agafa el HashMap
	@Override
	public HashMap<Integer, Client> getMap() {
		
		//Retorna el HashMap
		return clients;
		
	}
	
	public void mostrar(){

		System.out.println("-------------------");
		System.out.println("Clients");
		System.out.println("-------------------");
		
		for (Client c : clients.values()) {
		    System.out.println(c);
		}
	}
	
	//Guardar en un arxiu
	public void guardarFitxer() {
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("res/Clients.dat"))){
			
			//Escriu tot el HashMap a l'arxiu
			oos.writeObject(clients);
			
		} catch (Exception e) { //Altres errors
			
			System.out.println("\nUPS!! No shan pogut guardar els clients");
		}
	}
	
	//Carregar de l'arxiu
	public void obrirFitxer() {
		
		//Arxiu
		File f = new File("res/Clients.dat");
		if (f.exists()) { //Només si existeix
		
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){
				
				//Es carreguen els productes al HashMap
				clients = (HashMap<Integer, Client>)ois.readObject();			

			} catch (FileNotFoundException e) { //Arxiu no trobat
	
				//e.printStackTrace();
				System.out.println("\nUPS!! No sha trobat l'arxiu");
			
			}catch (Exception e) { //Altres errors
				
				//e.printStackTrace();
				System.out.println("\nUPS!! Alguna cosa ha fallat (Clients)");
				
			}
		}
	}

}
