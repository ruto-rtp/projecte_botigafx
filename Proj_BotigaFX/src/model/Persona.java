package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashSet;

public abstract class Persona implements Serializable{
	
	//ID
	private static final long serialVersionUID = 1L;
	
	
	//variables
	private int idpersona;
	private String dni;
	private String nom;
	private String cognom;
	private LocalDate dataNeix;
	private LinkedHashSet<String> telefons = new LinkedHashSet<>();	
	private String email;
	private Adreca direccion;
	public static int n_pers;

	
	//CONSTRUCTORS
	public Persona(int id, String dni, String nom, String cognom) {
		n_pers ++;
		idpersona = id;
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.dataNeix = null;
		this.telefons = null;
		this.email = "";
		this.direccion = null;
	}

	public Persona(int id, String dni, String nom, String cognom, LocalDate dataNeix, LinkedHashSet<String> telf, String mail, Adreca direccio) {
		n_pers ++;
		idpersona = id;
		this.dni = dni;
		this.nom = nom;
		this.cognom = cognom;
		this.dataNeix = dataNeix;
		telefons = telf;
		email = mail;
		direccion = direccio;
	}

	
	//GETTERS Y SETTERS
	public int getEdat() {
		int edat = 0;
		if (dataNeix != null) {
			edat = (int) ChronoUnit.YEARS.between(dataNeix, LocalDate.now());
			
		}
		return edat;
	}
	
	public static long diferenciaEdad(Persona p1, Persona p2) {
		long edat = ChronoUnit.YEARS.between(p1.getDataNeix(), p2.getDataNeix());
		return Math.abs(edat);
	}
		
	public static int getNumPersones() {
		return n_pers;
	}

	public void setIdpersona(int idpersona) {
		this.idpersona = idpersona;
	}

	public int getIdpersona() {
		return idpersona;
	}

	public void setDni(String DNI) {
		dni = DNI;
	}

	public String getDni() {
		return dni;
	}

	public void setNom(String NOM) {
		nom = NOM;
	}

	public String getNom() {
		return nom;
	}

	public void setCognom(String COGNOM) {
		cognom = COGNOM;
	}
	public String getCognom() {
		return cognom;
	}

	public void setDataNeix(LocalDate DATA) {
		dataNeix = DATA;
	}

	public LocalDate getDataNeix() {
		return dataNeix;
	}

	public void setEmail(String EMAIL) {
		email = EMAIL;
	}

	public String getEmail() {
		return email;
	}

	public void setTelefon(LinkedHashSet<String> TELF) {
		telefons = TELF;
	}

	public String getTelefon() {
		return telefons.toString().replace("[", "").replace("]", "");
	}

	public void setDireccion(Adreca direccion) {
		this.direccion = direccion;
	}

	public Adreca getDireccion() {
		return direccion;
	}
	
	
	//MÈTODES
	public Persona() {
		n_pers ++;
	}
	
	
	//PRINT
	public String toString() {
		return "ID:" + getIdpersona()
				+ "\nNom: " + getNom()
				+ "\nCognom: " + getCognom()
				+ "\nDNI: " + getDni()
				+ "\nData de naixement: " + getDataNeix()
				+ "\nEdat: " + getEdat()
				+ "\nTelefon: " + getTelefon()
				+ "\nCorreu: " +  getEmail()
				+ "\nDireccio: " + getDireccion()
				+ "\n\n";
	}

}
