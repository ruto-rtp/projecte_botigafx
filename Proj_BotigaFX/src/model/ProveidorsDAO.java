package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class ProveidorsDAO implements Persistable<Proveidor>{

	private static HashMap<Integer, Proveidor> proveidors = new HashMap<>();

	//Guardar proveidor
	@Override
	public Proveidor guardar(Proveidor obj) {

		//Guardar a HashMap
		proveidors.put(obj.getIdpersona(), obj);
		return null;
	}
	
	//Eliminar proveidor
	@Override
	public Proveidor eliminar(Integer id) {
		
		//Comprovació d'ID
		if (proveidors.containsKey(id)) {
			//Asigna el proveidor a una variable
			Proveidor  p = (Proveidor)proveidors.get(id);
			
			//Elimina el proveidor
			proveidors.remove(id);
			
			//Retorna el proveidor
			return p;
		}
		return null;
		
	}
	
	//Buscar proveidor
	@Override
	public Proveidor buscar(Integer id) {

		//Comprovació d'ID
		if (proveidors.containsKey(id)) {
			//Retorna el proveidor
			return proveidors.get(id);
		}
		return null;
		
	}
	
	//Agafa el HashMap
	@Override
	public HashMap<Integer, Proveidor> getMap() {
		
		//Retorna el HashMap
		return proveidors;
		
	}
	
	public void mostrar(){

		System.out.println("-------------------");
		System.out.println("Proveidors");
		System.out.println("-------------------");
		
		for (Proveidor p : proveidors.values()) {
		    System.out.println(p);
		}
	}
	
	//Guardar en un arxiu
	public void guardarFitxer() {
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("res/Proveidors.dat"))){
			
			//Escriu tot el HashMap a l'arxiu
			oos.writeObject(proveidors);
			
		} catch (Exception e) { //Altres errors
			
			System.out.println("\nUPS!! No shan pogut guardar els proveidors");
		}
	}
	
	//Carregar de l'arxiu
	public void obrirFitxer() {
		
		//Arxiu
		File f = new File("res/Proveidors.dat");
		if (f.exists()) { //Només si existeix
		
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){
				
				//Es carreguen els productes al HashMap
				proveidors = (HashMap<Integer, Proveidor>)ois.readObject();			

			} catch (FileNotFoundException e) { //Arxiu no trobat
	
				//e.printStackTrace();
				System.out.println("\nUPS!! No sha trobat l'arxiu");
			
			}catch (Exception e) { //Altres errors
				
				//e.printStackTrace();
				System.out.println("\nUPS!! Alguna cosa ha fallat (Proveidors)");
				
			}
		}
	}
}
