package model;

import java.util.Comparator;

public class ComparadorNom implements Comparator<Producte>{

	@Override
	public int compare(Producte p1, Producte p2) {
		return p1.getNom().compareTo(p2.getNom());
	}
	
}
