package model;

import java.util.HashMap;

public interface Persistable<T> {

	public T guardar(T obj);
	public T eliminar(Integer id);
	public T buscar(Integer id);
	public HashMap<Integer, T> getMap();
}
