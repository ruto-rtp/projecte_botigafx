/**
 * 
 */
package model;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author Pol Ruiz
 *
 * 1r Daw
 * 08-04-2023
 */
public class Presencia {

	//variables
	private int idtreballador;
	private LocalDate data;
	private LocalTime horaEnt;
	private LocalTime horaSort;

	
	//CONSTRUCTORS
	public Presencia() {
		
	}
	
	public Presencia(int idpersona, LocalDate data, LocalTime horaEnt, LocalTime horaSort) {
		this.idtreballador = idpersona;
		this.data = data;
		this.horaEnt = horaEnt;
		this.horaSort = horaSort;
	}
	
	//GETTERS Y SETTERS
	public int getIdtreballador() {
		return idtreballador;
	}


	public void setIdtreballador(int idpersona) {
		this.idtreballador = idpersona;
	}


	public LocalDate getData() {
		return data;
	}


	public void setData(LocalDate data) {
		this.data = data;
	}


	public LocalTime getHoraEnt() {
		return horaEnt;
	}


	public void setHoraEnt(LocalTime horaEnt) {
		this.horaEnt = horaEnt;
	}


	public LocalTime getHoraSort() {
		return horaSort;
	}


	public void setHoraSort(LocalTime horaSort) {
		this.horaSort = horaSort;
	}


	//PRINT
	public String toString() {
		return "ID:" + getIdtreballador()
				+ "\nData: " + getData()
				+ "\nH Entrada: " + getHoraEnt()
				+ "\nH Sortida: " + getHoraSort()
				+ "\n";
	}
}
