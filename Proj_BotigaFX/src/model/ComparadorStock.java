package model;

import java.util.Comparator;

public class ComparadorStock implements Comparator<Producte>{

	@Override
	public int compare(Producte p1, Producte p2) {
		return Integer.compare(p1.getStock(), p2.getStock());
	}
	
}
