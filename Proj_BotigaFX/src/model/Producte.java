package model;

import java.time.LocalDate;

import exception.StockInsuficientException;


public class Producte extends ProducteAbstract implements Comparable<Producte>{

	//ID
	private static final long serialVersionUID = 1L;
	
	
	//VARIABLES
	private int stock;
	private double preuVenda;
	private LocalDate iniciC;
	private LocalDate finalC;

	
	//CONSTRUCTORS
	public Producte() {
		super(-1, "");
	}
	
	public Producte(int id, String nom, int stock, double preuVenda, LocalDate iniciC, LocalDate finalC) {
		super(id, nom);

		this.stock = stock;
		this.preuVenda = preuVenda;
		this.iniciC = iniciC;
		this.finalC = finalC;
	}
	
	
	//GETERS I SETTERS
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public int getStock() {
		return stock;
	}

	public void setpreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public double getPreuVenda() {
		return preuVenda;
	}	
	
	public void setiniciC(LocalDate iniciC) {
		this.iniciC = iniciC;
	}

	public LocalDate getiniciC() {
		return iniciC;
	}
	
	public void setfinalC(LocalDate finalC) {
		this.finalC = finalC;
	}

	public LocalDate getFinalC() {
		return finalC;
	}
	
	
	//MÈTODES
	public void afegirStock(int sum) {
		stock += sum;
	}
	
	public void treureStock(int res) throws StockInsuficientException{
		
		if (res <= stock) {
			stock -= res;
		}else {
			throw new StockInsuficientException("UPS! L stock es massa petit");
		}
	}
	
	
	//PRINT
	@Override
	public String toString() {
		return "ID prod: " + getIdproducte()
				+ "\nNom prod: " + getNom()
				+ "\nPreu: " + getPreuVenda()
				+ "\nStock: " + getStock()
				+ "\nDescatalogament: " + getFinalC()
				+ "\n";
	}

	@Override
	public int compareTo(Producte o) {
		return 1;
	}
}
