package exception;

public class StockInsuficientException extends Exception{

	public StockInsuficientException() {
		super();
	}

	public StockInsuficientException(String msg) {
		super(msg);
	}
}
